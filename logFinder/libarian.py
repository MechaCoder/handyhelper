# Copyright (c) 2016 Copyright Holder All Rights Reserved.

#
# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# proudmanddandc@gmail.com wrote this file.  As long as you retain this notice
# you can do whatever you want with this stuff. If we meet some day, and you
# think this stuff is worth it, you can buy me a beer in return.
# MattyP
# ----------------------------------------------------------------------------
#

# import argparse
from argparse import ArgumentParser
from os import listdir


class Getlog():

    def __init__(self, logid):
        self.logId = logid
        self.logFiles = []
        for e in listdir("."):
            if '-error' in e:
                self.logFiles.append(e)
            pass

    def getfilecontent(self, filename=""):
        """ takes the name of a file in the same directory and returns an array
        of lines from the file
        """
        with open('./{}'.format(filename), mode='r') as log:
            return(log.readlines())
        pass

    def getLog(self):
        foundLogs = []
        for e in self.logFiles:
            for item in self.getfilecontent(e):
                if self.logId in item:
                    foundLogs.append("{} - [{}]".format(e, item.strip('\n')))
        # print(foundLogs)
        return foundLogs


if __name__ == '__main__':
    argsCont = ArgumentParser()
    argsCont.add_argument(
        "key_chain",
        help="enter a chain of keys to filter logs, e.g. the log ident",
        type=str
    )
    logObj = Getlog(argsCont.parse_args().key_chain)

    for e in logObj.getLog():
        print(e)
