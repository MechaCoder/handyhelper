# libarian.py

![](http://vignette3.wikia.nocookie.net/warhammer40k/images/6/60/Blood_Angels_Librarian.jpg/revision/latest?cb=20120523233058)


### use case
At work, I have been working on the logging for a web app, and a this is a result of a concern I have, that logs can quickly become unmanageable. And finding a record in a file that has a large user base can be difficult to find. Easily on a live (production server), in a similar way with it can be time-consuming to find logs entries as well disrupting the flow if the server admin is doing something else and developer pings to ask for records for example.

### how to use
this has been programmed only to use packages Python standard library, so there is no need to use a virtual environment or pip to install dependencies

all you need to do is to place this `libarian.py` in the same directory as the logfiles are created and then you can invoke libarian by useing the following command `sh- $ python3 libarian.py [key_chain]` what will happen is that script will look in the current directry for all files that have `-error` in the file name and looks in each file it finds and will display any log entry that has key chain, the script will not attempt to change any the files, you can also import libarian and use the `Getlog` class as part of a big system.

Copyright (c) 2016 Copyright Holder All Rights Reserved.
